#include <iostream>

#include "builder/baseBuilderCounter.hpp"
#include "builder/base.hpp"
#include "builder/director.hpp"

int main() {

    std::unique_ptr<Builder::elementHolder> data;
    Builder::Director buildMaker;
    Builder::countBaseBuilder* build = new Builder::countBaseBuilder();

    buildMaker.setBuilder(build);
    buildMaker.createElements();
    build->getCounts();

    build->transferHolder(data);
    data->printElements();

    delete build;

    return 0;
}
