#include <iostream>
#include <memory>

#include "adapter/adapt.hpp"

int main(){
    std::unique_ptr<Adapt::NewShape>
        shapeInh(new Adapt::ShapeAdapterInheritance(40,40,80,80,10));

    shapeInh->newPrintCoords();

    std::unique_ptr<Adapt::NewShape>
        shapeObj(new Adapt::ShapeAdapterObject(40,40,80,80,10));

    shapeObj->newPrintCoords();

    return 0;
}
