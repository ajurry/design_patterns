#include "facade/facade.hpp"
#include <memory>

int main() {

    std::unique_ptr<Facade::Facade> faPtr{new Facade::Facade()};
    faPtr->buildCarAndCost();
    return 0;
    
}
