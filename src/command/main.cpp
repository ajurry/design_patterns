#include <iostream>
#include <memory>

#include "command/cmdObjectStorage.hpp"
#include "command/cmdPrototype.hpp"
#include "command/command.hpp"

int main() {

    std::unique_ptr<Cmd::messageFactory>
        factory{new Cmd::messageFactory()};

    std::unique_ptr<Cmd::ObjectStorage> storage{new Cmd::ObjectStorage()};

    std::unique_ptr<Cmd::InsertCommand>
    insert{new Cmd::InsertCommand(storage.get(), factory.get())};

    insert->Execute(Cmd::messageComp::IMAGE, "IMAGE", 1);
    insert->Execute(Cmd::messageComp::IMAGE, "IMAGE", 2);
    storage->printObjectStorage();

    std::unique_ptr<Cmd::DeleteCommand>
    del{new Cmd::DeleteCommand(storage.get(), factory.get())};

    std::unique_ptr<Cmd::MacroInsertCommand>
    macroinsert{new Cmd::MacroInsertCommand(storage.get(), factory.get())};

    del->Execute("IMAGE1");
    del->Execute("IMAGE2");

    macroinsert->Execute(Cmd::messageComp::WORD, "WORD", 20);
    storage->printObjectStorage();

    return 0;
}
