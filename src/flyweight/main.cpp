#include <iostream>
#include <memory>
#include "flyweight/flyweight.hpp"

int main() {

    std::unique_ptr<Flyweight::AsteroidFactory>
        ptr{new Flyweight::AsteroidFactory};

    std::unique_ptr<Flyweight::AsteroidObject>
        a1ptr{new Flyweight::AsteroidObject(2,1,1,ptr.get())};

    std::unique_ptr<Flyweight::AsteroidObject>
        a2ptr{new Flyweight::AsteroidObject(2,0,0,ptr.get())};

    a1ptr->printVerticies();
    a2ptr->printVerticies();

    return 0;
}
