#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include <random>
#include "prototype/protoFactory.hpp"

int main ()
{
    Prototype::rgbFactory rgbFact;

    std::vector<std::unique_ptr<Prototype::protoColour> > store;

    std::mt19937 rng;
    std::random_device ran;
    rng.seed(ran());
    std::uniform_int_distribution<std::mt19937::result_type> gen(1,3);

    auto i=0;
    while(i < 5){
        int randColour = gen(rng);
        if(randColour==1){
            store.emplace_back(rgbFact.createColour(Prototype::colours::RED));
        } else if(randColour==2){
            store.emplace_back(rgbFact.createColour(Prototype::colours::GREEN));
        } else if(randColour==3){
            store.emplace_back(rgbFact.createColour(Prototype::colours::BLUE));
        }
        ++i;
    }

    std::for_each(store.begin(),
                    store.end(),
                    [](const std::unique_ptr<Prototype::protoColour>& elem){
                        elem->printColour();
                    });

    return 0;
}
