#include <iostream>
#include <memory>
#include "bridge/bridge.hpp"

int main(){
    std::cout<<"Begin"<<std::endl;
    std::unique_ptr<Bridge::Shape> ptr{new Bridge::Shape()};

    ptr->addVerts(std::tuple<int, int>(1,2));
    ptr->addVerts(std::tuple<int, int>(3,4));
    ptr->addVerts(std::tuple<int, int>(5,6));

    ptr->printVerts();
}
