#include "proxy/numproxy.hpp"
#include <iostream>
#include <memory>

int main() {

    std::cout<<"Start"<<std::endl;
    std::unique_ptr<Num::NumProxy>
        ptr{new Num::NumProxy(1,10,50,Num::Distributions::UNIFORM)};
    std::cout<<"Created Proxy"<<std::endl;

    ptr->printNum();

    return 0;
}
