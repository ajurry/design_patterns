#include <string>
#include <iostream>
#include <memory>

#include "chain/handlerHelp.hpp"
#include "chain/application.hpp"
#include "chain/button.hpp"
#include "chain/icon.hpp"



int main() {

    std::unique_ptr<Front::Application> application{new Front::Application(Handler::HandlerToken::NONE)};
    std::unique_ptr<Objects::Icon> icon{new Objects::Icon(application.get())};
    std::unique_ptr<Objects::Button> button{new Objects::Button(icon.get())};

    button->handleHelp();
    icon->handleHelp();
    application->handleHelp();

    button->setHandlerToken(Handler::HandlerToken::NONE);
    icon->setHandlerToken(Handler::HandlerToken::NONE);
    button->handleHelp();

}
