#include <iostream>
#include <memory>

#include "decorator/decorator.hpp"

int main(){

    std::unique_ptr<Decorator::TerminalOutput>
        toPtr{new Decorator::TerminalOutput()};

    toPtr->setContent("Content");

    std::unique_ptr<Decorator::BorderDecorator>
        bdPtr{new Decorator::BorderDecorator(toPtr.get(),'@')};

    toPtr->printOutput();
    std::cout<<std::endl;
    bdPtr->printOutput();
    std::cout<<std::endl;
    

    return 0;
}
