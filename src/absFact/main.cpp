#include <iostream>
#include <memory>
#include <absFactory/abstractFactory.hpp>
#include <absFactory/genericItem.hpp>

class client{
public:
    client(absFactEx::absFactory* factory)
    : _factory{factory}
    {}

public:
    void makePrint()
    {
        std::unique_ptr<genItem::Item> A = _factory->createWidget(1,2);
        std::unique_ptr<genItem::Item> B = _factory->createWidget(3,4);

        A->printRefShort();
        B->printRefShort();

        A->printRef();
        B->printRef();
    }

private:
    absFactEx::absFactory* _factory;
};

typedef std::unique_ptr<absFactEx::absFactory> fPtr;

int main(){

    fPtr absfact{new absFactEx::widgetFactoryA};
    std::unique_ptr<client> C = std::make_unique<client>(client(absfact.get()));
    C->makePrint();

    absfact = fPtr{new absFactEx::widgetFactoryB};
    C = std::make_unique<client>(client(absfact.get()));
    C->makePrint();

    return 0;
}
