#ifndef FLYWEIGHT_HPP
#define FLYWEIGHT_HPP

#include <iostream>
#include <unordered_map>
#include <tuple>
#include <vector>
#include <memory>
#include <cmath>

namespace Flyweight{

class Asteroid{

public:
    Asteroid(int radius);

public:
    void createAsteroidVerts();
    const std::vector<std::tuple<float, float> >& getVerts() const {return _verts;}

private:
    int _radius;
    int _polyNumber = 10;
    std::vector<std::tuple<float, float> > _verts;
};

Asteroid::Asteroid(int radius)
:_radius{radius}
{
    createAsteroidVerts();
}

void Asteroid::createAsteroidVerts()
{
    float x;
    float y;

    for(auto i=0; i<_polyNumber; ++i)
    {
        float fract = static_cast<float>(i)/static_cast<float>(_polyNumber);
        x = static_cast<float>(_radius)*cos(fract*2*M_PI);
        y = static_cast<float>(_radius)*sin(fract*2*M_PI);
        _verts.emplace_back(std::make_tuple(x,y));
    }

    return;
}

class AsteroidFactory{

public:
    AsteroidFactory() = default;

public:
    const std::unique_ptr<Asteroid>& getAsteroid(int radius);

private:
    std::unordered_map<float, std::unique_ptr<Asteroid>> _asteroidTemps;
};

const std::unique_ptr<Asteroid>& AsteroidFactory::getAsteroid(int radius){

    auto search = _asteroidTemps.find(radius);
    if(search == _asteroidTemps.end()){
        std::cout<<"Making Asteroid Of Radius: "<<radius<<std::endl;
        _asteroidTemps.insert(
            std::make_pair(radius, std::unique_ptr<Asteroid>{new Asteroid(radius)})
        );
    }
return _asteroidTemps[radius];
}


class AsteroidObject{
public:
    AsteroidObject(int radius ,float centreX, float centreY, AsteroidFactory* factory)
    : _radius{radius}, _centreX{centreX}, _centreY{centreY}, _factory{factory}
    {}

    void printVerticies();

private:
    int _radius;
    float _centreX;
    float _centreY;
    AsteroidFactory* _factory;
};

void AsteroidObject::printVerticies(){

    const std::vector<std::tuple<float, float> >&
        astVert{_factory->getAsteroid(_radius)->getVerts()};

    for(auto iter = astVert.begin(); iter!=astVert.end(); ++iter){
        std::cout<<"X: "<<std::get<0>(*iter)+_centreX<<"\t"
        <<"Y: "<<std::get<1>(*iter)+_centreY<<std::endl;
    }
}
};


#endif
