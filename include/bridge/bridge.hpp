#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <iostream>
#include <vector>
#include <memory>

#include "bridge/bridgeImp.hpp"

namespace Bridge{

class Shape{
public:
    Shape()
    :_imp{new ShapeImp}
    {}

    virtual void printVerts(){_imp->printVerts();}
    virtual void addVerts(std::tuple<int, int> vert){_imp->addVerts(vert);}

    virtual ~Shape(){}
protected:
private:
    std::unique_ptr<ShapeImp> _imp;
};

};

#endif
