#ifndef SHAPEIMP_HPP
#define SHAPEIMP_HPP

#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>

namespace Bridge{

class Data{
public:
    Data() = default;
    void addVerts(std::tuple<int, int> vert);
    void printVerts();
private:
    std::vector<std::tuple<int,int>> _verts;
};

void Data::addVerts(std::tuple<int, int> vert)
{
    _verts.emplace_back(vert);
}

void Data::printVerts()
{
    std::cout<<"x"<<"\t"<<"y"<<std::endl;
    for_each(_verts.begin(),_verts.end(),
        [](std::tuple<int, int> vert){
            std::cout<<std::get<0>(vert)<<"\t"<<std::get<1>(vert)<<std::endl;
        });
}

class ShapeImp{
public:
    ShapeImp()
    :_data{new Data}
    {}

    virtual void printVerts();
    virtual void addVerts(std::tuple<int, int> vert);

    virtual ~ShapeImp(){}
protected:
    const std::unique_ptr<Data>& getData() const;
private:
    std::unique_ptr<Data> _data;
};

void ShapeImp::printVerts()
{
    _data->printVerts();
}

void ShapeImp::addVerts(std::tuple<int, int> vert)
{
    _data->addVerts(vert);
}

};

#endif
