#include <iostream>
#include <memory>

namespace Adapt{

class NewShape{
public:
    virtual void newPrintCoords() = 0;
    virtual ~NewShape(){}
};

class Shape{
public:
    explicit Shape(int xBL, int yBL, int xTR, int yTR, int verts)
    :_xBLeft{xBL}, _yBLeft{yBL},
    _xTRight{xTR}, _yTRight{yTR},
     _verts{verts}
    {}

    void printCoords(){
        std::cout<<"("<<_xTRight<<","<<_yTRight<<")"
        " "<<"("<<_xBLeft<<","<<_yBLeft<<")"
        <<" "<<"vertices: "<<_verts<<std::endl;
    }
private:
    int _xBLeft;
    int _yBLeft;
    int _xTRight;
    int _yTRight;
    int _verts;
};

class ShapeAdapterInheritance : public NewShape, private Shape{
public:
    explicit ShapeAdapterInheritance(int xBL, int yBL, int width, int height, int verts)
    :Shape(xBL,yBL,xBL+width,yBL+height,verts)
    {
    }

    void newPrintCoords(){
        std::cout<<"Printing Coords"<<std::endl;
        printCoords();
    }

    virtual ~ShapeAdapterInheritance() {}
};

class ShapeAdapterObject : public NewShape{
public:
    explicit ShapeAdapterObject(int xBL, int yBL, int width, int height, int verts)
    :_shape{new Shape(xBL, yBL, xBL+width, yBL+height,verts)}
    {}

    void newPrintCoords(){
        std::cout<<"Printing Coords"<<std::endl;
        _shape->printCoords();
    }

    virtual ~ShapeAdapterObject(){}
private:
    std::unique_ptr<Shape> _shape;
};
};
