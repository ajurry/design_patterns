#include <iostream>
#include <memory>
#include <unordered_map>

namespace Prototype{

enum class colours{RED, GREEN, BLUE};

class protoColour{
public:
    virtual std::unique_ptr<protoColour> clone() = 0;
    virtual void printColour() = 0;
};

class protoRed : public protoColour{
public:
    std::unique_ptr<protoColour> clone()
    {
        return std::make_unique<protoRed>(*this);
    }
    void printColour() {std::cout<<"Red"<<std::endl;}
};

class protoGreen : public protoColour{
public:
    std::unique_ptr<protoColour> clone()
    {
        return std::make_unique<protoGreen>(*this);
    }
    void printColour() {std::cout<<"Green"<<std::endl;}
};

class protoBlue : public protoColour{
public:
    std::unique_ptr<protoColour> clone()
    {
        return std::make_unique<protoBlue>(*this);
    }
    void printColour() {std::cout<<"Blue"<<std::endl;}
};

class rgbFactory{
private:
    std::unordered_map<colours, std::unique_ptr<protoColour> > mColours;
public:
    rgbFactory()
    {
        mColours[colours::RED] = std::make_unique<protoRed>();
        mColours[colours::GREEN] = std::make_unique<protoGreen>();
        mColours[colours::BLUE] = std::make_unique<protoBlue>();
    }

    std::unique_ptr<protoColour> createColour(colours colour)
    {
        return mColours[colour]->clone();
    }

};

};
