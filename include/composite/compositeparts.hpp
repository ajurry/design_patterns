#ifndef COMPOSITE_PARTS_HPP
#define COMPOSITE_PARTS_HPP

#include <iostream>
#include <string>
#include <list>
#include <memory>

#include "composite/parts.hpp"

namespace Composite{

class CompositePart : public Parts{
public:
    virtual const float costParts();
    virtual void addParts(Parts* ptr);
    virtual void removeParts(Parts* ptr);
    virtual ~CompositePart(){}
protected:
    CompositePart(std::string name)
    :Parts(name)
    {}
private:
    std::list<Parts*> _parts;
};

void CompositePart::addParts(Parts* ptr)
{
    _parts.push_back(ptr);
    return;
}

void CompositePart::removeParts(Parts* ptr)
{
    auto iter = std::find(_parts.begin(),_parts.end(), ptr);
    _parts.erase(iter);
}

const float CompositePart::costParts()
{
    float total = 0;

    for(auto iter = _parts.begin(); iter!=_parts.end(); ++iter){
        total += (*iter)->costParts();
    }

    return total;
}

class Car : public CompositePart{
public:
    Car(std::string name)
    :CompositePart(name)
    {}
    virtual ~Car(){}
};

class Hood : public CompositePart{
public:
    Hood(std::string name)
    :CompositePart(name)
    {}
    virtual ~Hood(){}
};

class Front : public CompositePart{
public:
    Front(std::string name)
    :CompositePart(name)
    {}
    virtual ~Front(){}
};

class Back : public CompositePart{
public:
    Back(std::string name)
    :CompositePart(name)
    {}
    virtual ~Back(){}
};

class Boot : public CompositePart{
public:
    Boot(std::string name)
    :CompositePart(name)
    {}
    virtual ~Boot(){}
};

};

#endif
