#ifndef PARTS_HPP
#define PARTS_HPP

#include <iostream>
#include <algorithm>
#include <string>
#include <memory>
#include <list>

namespace Composite{

class Parts{
public:
    virtual const float costParts();
    virtual void addParts(Parts* ptr);
    virtual void removeParts(Parts* ptr);
    std::string getName(){return _name;}
    virtual ~Parts(){}
protected:
    explicit Parts(std::string name)
    :_name{name}
    {}
private:
    const std::string _name;
    std::list<Parts*> _parts;
};

void Parts::removeParts(Parts* ptr)
{
    auto iter = std::find(_parts.begin(),_parts.end(), ptr);
    _parts.erase(iter);
}

void Parts::addParts(Parts* ptr)
{
    _parts.push_back(ptr);
    return;
}

const float Parts::costParts()
{
    float total = 0;

    for(auto iter = _parts.begin(); iter!=_parts.end(); ++iter){
        total += (*iter)->costParts();
    }

    return total;
}

class Window : public Parts{
public:
    Window(std::string name, float price)
    :Parts(name), _price{price}
    {}

    virtual const float costParts() {return _price;}
private:
    const float _price;
};

class Speaker : public Parts{
public:
    Speaker(std::string name, float price)
    :Parts(name), _price{price}
    {}

    virtual const float costParts() {return _price;}
private:
    const float _price;
};

class Seat : public Parts{
public:
    Seat(std::string name, float price)
    :Parts(name), _price{price}
    {}

    virtual const float costParts() {return _price;}
private:
    const float _price;
};

class Engine : public Parts{
public:
    Engine(std::string name, float price)
    :Parts(name), _price{price}
    {}

    virtual const float costParts() {return _price;}
private:
    const float _price;
};

class Tank : public Parts{
public:
    Tank(std::string name, float price)
    :Parts(name), _price{price}
    {}

    virtual const float costParts() {return _price;}
private:
    const float _price;
};

};

#endif
