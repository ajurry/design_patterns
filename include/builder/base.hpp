#ifndef BASE_HPP
#define BASE_HPP

#include <vector>
#include <iostream>

namespace Builder{

class elementHolder{
public:
    elementHolder() = default;
    elementHolder(const elementHolder& eleH);

    void printElements();

    const std::vector<char> getElementStorage() const {return _elementStorage;}
    void addElement(char element) {_elementStorage.emplace_back(element);}

private:
    std::vector<char> _elementStorage;
};

elementHolder::elementHolder(const elementHolder& eleH)
{
    this->_elementStorage = eleH.getElementStorage();
}

void elementHolder::printElements()
{
    for(auto i = this->_elementStorage.begin(); i!= this->_elementStorage.end(); ++i)
    {
        std::cout<<*i<<' ';
    }
    std::cout<<std::endl;
}

};

#endif
