#ifndef BASEBUILDER_HPP
#define BASEBUILDER_HPP

#include "base.hpp"
#include <iostream>
#include <memory>

namespace Builder{

class baseBuilder{
public:
    virtual void buildElementA() {}
    virtual void buildElementB() {}
    virtual void buildElementC() {}

    virtual void transferHolder(std::unique_ptr<elementHolder>& trans) {return;}

    virtual ~baseBuilder(){}
private:
};

};

#endif
