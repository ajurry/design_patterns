#ifndef DIRECTOR_HPP
#define DIRECTOR_HPP

#include <vector>
#include <iostream>
#include "baseBuilder.hpp"
#include "base.hpp"

namespace Builder{

class Director{
public:
    void setBuilder(baseBuilder* builder)
    {
        _builder = builder;
    }
    void createElements();
private:
    baseBuilder* _builder;
};

void Director::createElements(){
    _builder->buildElementA();
    _builder->buildElementB();
    _builder->buildElementC();
    _builder->buildElementA();
    _builder->buildElementB();
    _builder->buildElementC();
    return;
}


};

#endif
