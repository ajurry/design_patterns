#ifndef BASEBUILDERCOUNTER_HPP
#define BASEBUILDERCOUNTER_HPP

#include "baseBuilder.hpp"
#include "base.hpp"

#include <iostream>
#include <memory>

namespace Builder{

class countBaseBuilder : public baseBuilder {
public:
    countBaseBuilder();

    virtual void buildElementA();
    virtual void buildElementB();
    virtual void buildElementC();

    void getCounts() const;

    virtual void transferHolder(std::unique_ptr<elementHolder>& trans);
    virtual ~countBaseBuilder(){}

private:
    std::unique_ptr<elementHolder> _currentElementHolder;

    int _elementA;
    int _elementB;
    int _elementC;
};

countBaseBuilder::countBaseBuilder()
:_currentElementHolder{new elementHolder},
_elementA{0}, _elementB{0}, _elementC{0}
{}

void countBaseBuilder::buildElementA()
{
    _elementA++;
    _currentElementHolder->addElement('A');
}

void countBaseBuilder::buildElementB()
{
    _elementB++;
    _currentElementHolder->addElement('B');
}

void countBaseBuilder::buildElementC()
{
    _elementC++;
    _currentElementHolder->addElement('C');
}

void countBaseBuilder::getCounts() const
{
    std::cout<<"A: "<<_elementA<<" B: "<<_elementB<<" C: "<<_elementC<<std::endl;
}

void countBaseBuilder::transferHolder(std::unique_ptr<elementHolder>& trans){
    trans = std::move(this->_currentElementHolder);
    return;
}

};


#endif
