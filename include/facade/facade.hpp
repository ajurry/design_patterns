#ifndef FACADE_HPP
#define FACADE_HPP

#include <iostream>
#include "composite/compositeparts.hpp"
#include "composite/parts.hpp"
#include <memory>

namespace Facade{

class Facade{
public:
    Facade() = default;

    virtual void buildCarAndCost();

    virtual ~Facade(){}
};

void Facade::buildCarAndCost()
{
    std::unique_ptr<Composite::Parts> cPtr{new Composite::Car("car1")};

    std::unique_ptr<Composite::Parts> hPtr{new Composite::Hood("hood1")};
    std::unique_ptr<Composite::Parts> ePtr1{new Composite::Engine("engine1", 500)};
    hPtr->addParts(ePtr1.get());

    std::unique_ptr<Composite::Parts> frPtr{new Composite::Front("front1")};
    std::unique_ptr<Composite::Parts> wPtr1{new Composite::Window("win1", 50)};
    std::unique_ptr<Composite::Parts> wPtr2{new Composite::Window("win2", 50)};
    frPtr->addParts(wPtr1.get());
    frPtr->addParts(wPtr2.get());

    std::unique_ptr<Composite::Parts> baPtr{new Composite::Back("back1")};
    std::unique_ptr<Composite::Parts> wPtr3{new Composite::Window("win3", 50)};
    std::unique_ptr<Composite::Parts> wPtr4{new Composite::Window("win4", 50)};
    baPtr->addParts(wPtr3.get());
    baPtr->addParts(wPtr4.get());

    std::unique_ptr<Composite::Parts> bPtr{new Composite::Boot("boot1")};
    std::unique_ptr<Composite::Parts> spPtr1{new Composite::Speaker("speaker1", 100)};
    std::unique_ptr<Composite::Parts> spPtr2{new Composite::Speaker("speaker2", 100)};
    std::unique_ptr<Composite::Parts> tPtr1{new Composite::Tank("tank1", 200)};
    bPtr->addParts(spPtr1.get());
    bPtr->addParts(spPtr2.get());
    bPtr->addParts(tPtr1.get());

    cPtr->addParts(hPtr.get());
    cPtr->addParts(frPtr.get());
    cPtr->addParts(baPtr.get());
    cPtr->addParts(bPtr.get());

    std::cout<<cPtr->costParts()<<std::endl;

    return;
}

};

#endif
