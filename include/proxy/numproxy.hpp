#ifndef NUMPROXY_HPP
#define NUMPROXY_HPP

#include "proxy/numbase.hpp"
#include "proxy/numdistributions.hpp"

#include <memory>
#include <iostream>

namespace Num{

class NumProxy : public NumBase{
public:
    NumProxy(int start, int finish, int number, Distributions dist )
    :_start{start}, _finish{finish}, _number{number}, _dist{dist}
    {
        std::cout<<"Constructing NumProxy"<<std::endl;
    }

    virtual void printNum();

    virtual ~NumProxy(){}


private:
    int _start;
    int _finish;
    int _number;
    Distributions _dist;

    std::unique_ptr<NumBase> _distribution;
};

void NumProxy::printNum(){
    if(_distribution == nullptr){
        switch(_dist){
            case Distributions::UNIFORM:
                _distribution
                    = std::unique_ptr<NumBase>{new NumUniform(_start, _finish, _number)};
        }
    }

    std::cout<<"Printing Data"<<std::endl;
    _distribution->printNum();
}

};

#endif
