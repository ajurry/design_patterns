#ifndef NUMDISTRIBUTIONS_HPP
#define NUMDISTRIBUTIONS_HPP

#include "proxy/numbase.hpp"
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <random>

namespace Num{

enum class Distributions {UNIFORM};

class NumUniform : public NumBase {
public:
    NumUniform(int start, int finish, int number);

    virtual void printNum();
    virtual ~NumUniform(){}
private:
    std::vector<int> _dist;
};

NumUniform::NumUniform(int start, int finish, int number)
{
    std::cout<<"Constructing NumUniform"<<std::endl;

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(start,finish);

    while(number > 0){
        _dist.emplace_back(distribution(generator));
        --number;
    }
}

void NumUniform::printNum()
{
    std::copy(_dist.begin(),
                    _dist.end(),
                    std::ostream_iterator<int>(std::cout,"\t"));

    std::cout<<std::endl;
}

}
#endif
