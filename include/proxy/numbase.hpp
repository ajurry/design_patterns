#ifndef NUMBASE_HPP
#define NUMBASE_HPP

namespace Num{

class NumBase{
public:
    virtual ~NumBase(){}
    virtual void printNum() = 0;
protected:
    NumBase() = default;
};

};

#endif
