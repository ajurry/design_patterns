#ifndef HANDLER_HELP_HPP
#define HANDLER_HELP_HPP

namespace Handler{

enum class HandlerToken {BUTTON, ICON ,NONE};

class HandlerHelp{
public:
    HandlerHelp(HandlerHelp* = 0, HandlerToken = HandlerToken::NONE);

    virtual bool hasHelp() {return _token != HandlerToken::NONE;}
    virtual void setHandlerToken(HandlerToken);

    virtual void handleHelp();

private:
    HandlerHelp* _successor;
    HandlerToken _token;
};

HandlerHelp::HandlerHelp(HandlerHelp* successor, HandlerToken token)
: _successor{successor}, _token{token}
{}

void HandlerHelp::setHandlerToken(Handler::HandlerToken token)
{
    _token = token;
}

void HandlerHelp::handleHelp(){
    if(_successor!=0) _successor->handleHelp();
}

};

#endif
