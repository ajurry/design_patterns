#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "handlerHelp.hpp"

#include <string>
#include <iostream>

namespace Front{

using namespace Handler;

class Application : public HandlerHelp {
public:
    Application(HandlerToken token = HandlerToken::NONE, std::string id="");
    virtual void handleHelp();

private:
    std::string _id;
};

Application::Application(HandlerToken token, std::string id)
: HandlerHelp(0, token), _id{id}
{}

void Application::handleHelp(){
    std::cout<<"Application Contains Elements Icon and Button"<<std::endl;
    std::cout<<"Help can be provided for both Elements"<<std::endl;
}

};

#endif
