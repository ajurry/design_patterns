#ifndef CMDPROTOTYPE_HPP
#define CMDPROTOTYPE_HPP

#include <iostream>
#include <unordered_map>
#include <memory>
#include <string>

namespace Cmd{

enum class messageComp{WORD, IMAGE};

class protoMessage{
public:
    virtual std::unique_ptr<protoMessage> clone(std::string info, int id) = 0;
    virtual void printInfo() = 0;
    virtual const std::string getID() = 0;
};

class wordPrototype : public protoMessage{
public:
    std::unique_ptr<protoMessage> clone(std::string info, int id);
    void printInfo() {std::cout<<"ID: "<<_id<<" "<<_word<<std::endl;}
    const std::string getID() {return _id;}
private:
    std::string _word;
    std::string _id;
};

std::unique_ptr<protoMessage> wordPrototype::clone(std::string info, int id)
{
    _word = info;
    _id = "WORD" + std::to_string(id);
    return std::make_unique<wordPrototype>(*this);
}

class imagePrototype : public protoMessage{
public:
    std::unique_ptr<protoMessage> clone(std::string info, int id);
    void printInfo() {std::cout<<"ID: "<<_id<<" "<<_image<<std::endl;}
    const std::string getID(){return _id;}
private:
    std::string _image;
    std::string _id;
};

std::unique_ptr<protoMessage> imagePrototype::clone(std::string info, int id)
{
    _image = info;
    _id = "IMAGE" + std::to_string(id);
    return std::make_unique<imagePrototype>(*this);
}

class messageFactory{
public:
    messageFactory()
    {
        messages[messageComp::WORD] = std::make_unique<wordPrototype>();
        messages[messageComp::IMAGE] = std::make_unique<imagePrototype>();
    }

    std::unique_ptr<protoMessage>
    createMessageComponent(messageComp mComp, std::string info, int id)
    {
        return messages[mComp]->clone(info, id);
    }
private:
    std::unordered_map<messageComp, std::unique_ptr<protoMessage> > messages;
};

};

#endif
