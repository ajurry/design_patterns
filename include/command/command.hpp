#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <iostream>
#include <string>

#include "command/cmdPrototype.hpp"
#include "command/cmdObjectStorage.hpp"

namespace Cmd{

class Command{
public:
    Command() = default;
    virtual ~Command(){}
};

class DeleteCommand : public Command{
public:
    DeleteCommand(Cmd::ObjectStorage* storage, Cmd::messageFactory* factory)
    :_storage{storage}, _factory{factory} {}
    void Execute(std::string id);
private:
    Cmd::ObjectStorage* _storage;
    Cmd::messageFactory* _factory;
};

void DeleteCommand::Execute(std::string id){
    _storage->removeObject(id);
}

class InsertCommand : public Command{
public:
    InsertCommand(Cmd::ObjectStorage* storage, Cmd::messageFactory* factory)
    :_storage{storage}, _factory{factory} {}
    virtual void Execute(Cmd::messageComp type, std::string info, int id);
protected:
    void addMember(Cmd::messageComp type, std::string info, int id);
private:
    Cmd::ObjectStorage* _storage;
    Cmd::messageFactory* _factory;
};

void InsertCommand::addMember(Cmd::messageComp type, std::string info, int id){
    std::unique_ptr<Cmd::protoMessage> tmp{_factory->createMessageComponent(type, info, id)};
    _storage->addObject(tmp);
}

void InsertCommand::Execute(Cmd::messageComp type, std::string info, int id){
    addMember(type, info, id);
}

class MacroInsertCommand : public InsertCommand{
public:
    MacroInsertCommand(Cmd::ObjectStorage* storage, Cmd::messageFactory* factory)
    :InsertCommand(storage, factory) {}
    void Execute(Cmd::messageComp type, std::string info, int n);
};

void MacroInsertCommand::Execute(Cmd::messageComp type, std::string info, int n)
{
    for(auto i=0; i<n; ++i){
        addMember(type, info, i);
    }
}

};

#endif
