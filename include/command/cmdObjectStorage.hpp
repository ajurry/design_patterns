#ifndef CMD_OBJECT_STORAGE_HPP
#define CMD_OBJECT_STORAGE_HPP

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <memory>

#include "command/cmdPrototype.hpp"

namespace Cmd{

class ObjectStorage{
public:
    void addObject(std::unique_ptr<protoMessage>& composition);
    void removeObject(std::string id);
    void printObjectStorage();
private:
    std::vector< std::unique_ptr<protoMessage>> _objStore;
};

void ObjectStorage::addObject(std::unique_ptr<protoMessage>& composition)
{
    _objStore.push_back(std::move(composition));
}

void ObjectStorage::removeObject(std::string id)
{
    auto pos = std::find_if(_objStore.begin(),_objStore.end(),
                [&id](const std::unique_ptr<protoMessage>& elem){
                    return elem->getID() == id;
                });

    if(pos!=_objStore.end()){
        _objStore.erase(pos);
    } else {
        std::cout<<"There is no element with that ID"<<std::endl;
    }

    return;
}

void ObjectStorage::printObjectStorage(){
    std::for_each(_objStore.begin(),
                    _objStore.end(),
                    [](const std::unique_ptr<protoMessage>& elem){
                        elem->printInfo();
                    });

    if(_objStore.empty()){
        std::cout<<"No Elements"<<std::endl;
    }
}
};

#endif
