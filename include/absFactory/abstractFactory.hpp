#ifndef ABSTRACT_FACTORY_HPP
#define ABSTRACT_FACTORY_HPP

#include <absFactory/genericItem.hpp>

#include <iostream>

namespace absFactEx{

class absFactory{
public:
    virtual std::unique_ptr<genItem::Item> createWidget(int ref, int ref2) = 0;
    virtual ~absFactory(){}
private:
};

class widgetFactoryA : public absFactory{
    std::unique_ptr<genItem::Item> createWidget(int ref, int ref2){
        return std::unique_ptr<genItem::Item>(new genItem::widgetA{ref, ref2});
    }
    ~widgetFactoryA(){}
};

class widgetFactoryB : public absFactory{
    std::unique_ptr<genItem::Item> createWidget(int ref, int ref2){
        return std::unique_ptr<genItem::Item>(new genItem::widgetB{ref, ref2});
    }
    ~widgetFactoryB(){}
};

};
#endif
