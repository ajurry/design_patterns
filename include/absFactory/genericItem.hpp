#ifndef GENERIC_ITEM_HPP
#define GENERIC_ITEM_HPP

#include <boost/lexical_cast.hpp>
#include <iostream>
#include <string>

namespace genItem {

class Item{
public:
    explicit Item(int ref)
    :_idItem{"Item" + boost::lexical_cast<std::string>(ref)}
    {}

    Item& operator=(const Item&) = delete;
    Item(const Item&) = delete;
public:
    virtual void printRef() = 0;
    virtual void printRefShort() = 0;
    const std::string& getID(){return _idItem;}
private:
    std::string _idItem;
};

class widgetA : public Item{
public:
    explicit widgetA(int ref, int ref2)
    :Item(ref), _id{boost::lexical_cast<std::string>(ref2)}
    {}

    widgetA& operator=(const widgetA&) = delete;
    widgetA(const widgetA&) = delete;
public:
    void printRef(){std::cout<<"A"<<"-"<<_id<<"-"<<getID()<<std::endl;}
    void printRefShort(){std::cout<<"A"<<"-"<<_id<<std::endl;}
private:
    std::string _id;
};

class widgetB : public Item{
public:
    explicit widgetB(int ref, int ref2)
    :Item(ref), _id{boost::lexical_cast<std::string>(ref2)}
    {}

    widgetB& operator=(const widgetB&) = delete;
    widgetB(const widgetB&) = delete;
public:
    void printRef(){std::cout<<"B"<<"-"<<_id<<"-"<<getID()<<std::endl;}
    void printRefShort(){std::cout<<"B"<<"-"<<_id<<std::endl;}
private:
    std::string _id;
};
};
#endif
