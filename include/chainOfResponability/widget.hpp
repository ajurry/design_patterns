#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "handlerHelp.hpp"

#include <string>

namespace Objects{

using namespace Handler;

class Widget : public HandlerHelp{
protected:
    Widget(HandlerHelp* parent, HandlerToken token = HandlerToken::NONE);

private:
    HandlerHelp* _parent;
};

Widget::Widget(HandlerHelp* parent, HandlerToken token) : HandlerHelp(parent, token)
{
    _parent = parent;
}

};

#endif
