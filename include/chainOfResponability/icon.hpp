#ifndef ICON_HPP
#define ICON_HPP

#include "handlerHelp.hpp"
#include "widget.hpp"

#include <string>
#include <iostream>

namespace Objects{

using namespace Handler;

class Icon : public Widget {
public:
    Icon(Widget* parent, HandlerToken token = HandlerToken::BUTTON, std::string id="");
    virtual void handleHelp();

private:
    std::string _id;

};

Icon::Icon(Widget* parent, HandlerToken token, std::string id)
: Widget(parent, token), _id{id}
{}

void Icon::handleHelp(){
    if(hasHelp()){
        std::cout<<"ID: "<<_id<<std::endl;
        std::cout<<"An Icon to be Displayed"<<std::endl;
    } else {
        HandlerHelp::handleHelp();
    }
}

};

#endif
