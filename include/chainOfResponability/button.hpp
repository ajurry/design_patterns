#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "handlerHelp.hpp"
#include "widget.hpp"

#include <string>
#include <iostream>

namespace Objects{

using namespace Handler;

class Button : public Widget {
public:
    Button(Widget* parent, HandlerToken token = HandlerToken::BUTTON, std::string id="");
    virtual void handleHelp();

private:
    std::string _id;

};

Button::Button(Widget* parent, HandlerToken token, std::string id)
: Widget(parent, token), _id{id}
{}

void Button::handleHelp(){
    if(hasHelp()){
        std::cout<<"ID: "<<_id<<std::endl;
        std::cout<<"A button to be selected"<<std::endl;
    } else {
        HandlerHelp::handleHelp();
    }
}

void setHandlerToken(HandlerToken token){
    setHandlerToken(token);
}

};

#endif
