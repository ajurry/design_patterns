#ifndef DECORATOR_HPP
#define DECORATOR_HPP

#include <iostream>
#include <memory>
#include <string>

namespace Decorator{

class OutputStorage{
public:
    OutputStorage() = default;

    const std::string getString() const {return _output;}
    void setContnet(std::string str) {_output = str;}

    ~OutputStorage(){};
private:
    std::string _output;
};

class TerminalOutput{
public:
    TerminalOutput()
    :_outStore{new OutputStorage()}
    {}

    void setContent(std::string str){_outStore->setContnet(str);}
    virtual void printOutput(){std::cout<<_outStore->getString()<<std::endl;}
    virtual ~TerminalOutput(){};
private:
    std::unique_ptr<OutputStorage> _outStore;
};

class Decorator : public TerminalOutput{
public:
    Decorator(TerminalOutput* termOut)
    :_termOut{termOut}
    {}

    virtual void printOutput();

    virtual ~Decorator(){};
private:
    TerminalOutput* _termOut;
};

void Decorator::printOutput() {
    _termOut->printOutput();
}

class BorderDecorator : public Decorator{
public:
    BorderDecorator(TerminalOutput* termOut, char elementDec)
    :Decorator(termOut),_elementDec{elementDec}
    {}

    virtual void printOutput();
    virtual ~BorderDecorator(){};
private:
    void printBorder();
private:
    char _elementDec;
};

void BorderDecorator::printOutput() {
    printBorder();
    Decorator::printOutput();
    printBorder();
}

void BorderDecorator::printBorder() {
    std::string border;
    for(auto i = 0; i<20; ++i){
        border += _elementDec;
    }
    std::cout<<border<<std::endl;
}

};

#endif
